package stack;

public class Test {
	
	public static void main(String[] args){
		
		Stack<String> stack = new StackImpl<>();
		
		
		stack.push("Hello");
		stack.push("World");
		stack.push("xyz");
	//	stack.push(8);
	//	stack.push(9.5);
		
		System.out.println(stack.toList());
		if  (!stack.empty()){	
			String str = stack.pop();
			System.out.println(str);
		}
		if  (!stack.empty())
			System.out.println(stack.pop());
		if  (!stack.empty())
			System.out.println(stack.pop());
		if  (!stack.empty())
			System.out.println(stack.pop());
	}

}