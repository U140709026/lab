import java.util.List;
 
public interface Stack {
public interface Stack<T>{
 

	public void push(Object item);
	public void push(T item);
 	
 
	public Object pop ();
	public T pop ();
 	

 	public boolean empty();
 	

}
	public List<T> toList();
	
}