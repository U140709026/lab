package stack;
import java.util.ArrayList;
import java.util.List;
public class StackImpl<T> implements Stack<T> {
	StackItem<T> top;
	@Override
	public void push(T item) {

		StackItem<T> newTop = new StackItem<>(item,top);
		top = newTop;	}
	@Override
	public T pop() {
		
		T item= top.getItem();
		
		top = top.getPrevious();
		return item;
	}
	@Override
	public boolean empty() {

		return top == null;
	}

	@Override
	public List<T> toList() {
Add a comment to this line
		
		ArrayList<T> content = new ArrayList<T>();
		StackItem<T> current = top;
		while (current !=null){
			content.add(0,current.getItem());
			current =  current .getPrevious();	}
		return content;
	}}