package cagatay.shapes3d;

import cagatay.shapes.Circle;

public class Cylinder extends Circle {
	
	public Cylinder(){
		super(5);
		System.out.println("Cylinder is beign created");
	}
	 
	public Cylinder(int r){
		super(5);
		this.radius=r;
		System.out.println("Cylinder is beign created");
	}
	
}