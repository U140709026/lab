package Midterm;

public class Box_Question_12 extends Rectangle {
	
	int height;
	
	public Box_Question_12(int width, int length, int height ){
		 
		super  (width, length);
		this.height = height;
		
	}

	public double  area(){
		return 2 * super.area()  2 * length*height  2 * width*height;
	}
	public double volume(){
		return super.area() * height ;
	}
}